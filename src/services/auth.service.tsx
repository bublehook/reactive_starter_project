import { AsyncStorage } from "react-native";


export const getRawCredential = async (): Promise<string> => {
    try {
        const credential = await AsyncStorage.getItem("@credential")
        return credential
    } catch (error) {
        return ""
    }
};
;

export const hasCredential = async (): Promise<boolean> => {
    try {
        const rawCredential = await getRawCredential()
        return rawCredential ? rawCredential.length > 1 : false
    } catch (error) {
        return false
    }
};


import { Action, action , Thunk ,thunk } from "easy-peasy";


export interface UserInfo {
    id: string;
    createdAt: Date;
    name: string;
    avatar: string;
}

export interface AuthModel {
    user : UserInfo | null;
    signin: Thunk<AuthModel, string>;
}

const authModel : AuthModel = {
    user : null,
    signin: thunk((state, payload, {getState ,getStoreActions , getStoreState}) => {
        state.user = {}
            
    })
};

export default authModel;
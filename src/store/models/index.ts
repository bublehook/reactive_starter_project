
import test, { TestModel } from "./test";



export interface StoreModel {
    test: TestModel;
};


const model: StoreModel = {
    test
};
export default model;
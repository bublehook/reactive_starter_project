
import { Action, action, thunk, Thunk } from "easy-peasy";
import { create } from 'apisauce'
import { UserInfo } from "./auth";
import { userInfo } from "os";
import { stat } from "fs";


export interface TestModel {
    items: string[];
    newData: number;
    itemsAny: any[];
    users: UserInfo[];
    add: Action<TestModel, string>;
    updateUserItems: Action<TestModel, UserInfo[]>;
    fetch: Thunk<TestModel>;
}

const api = create({ baseURL: 'https://5e4fab5e43b2b200142a3b14.mockapi.io/v1/' })

const test: TestModel = {
    newData: 0,
    users: [],
    items: ["string A", "string B"],
    itemsAny: [{ a: "a" }, "x"],
    add: action((state, payload) => {
        state.items.push(payload);
    }),
    updateUserItems: action((state, payload) => {
        state.users = payload
    }),
    fetch: thunk(async (actions, payload, { getState }) => {
        try {
            const { data } = await api.get(`user/1`);
            actions.updateUserItems([(data as UserInfo)])
        } catch (error) {

        }

    })
};

export default test;

import * as React from 'react';
import { LoadingScreenProps } from "../../navigation/loading.navigator";
import { LayoutElement, Layout, Spinner } from "@ui-kitten/components";
import { Text, StyleSheet, Alert } from "react-native";
import { AppRoute } from '../../navigation/app-routes';




export const LoadingScreen = (props: LoadingScreenProps): LayoutElement => {

    const [countDown, setCountDown] = React.useState<number>(10)
    const [intervalId, setIntervalId] = React.useState()


    React.useEffect(() => {
        const interval = setInterval(() => {
            setIntervalId(interval)
            setCountDown(s => s - 1)

        }, 200)
        return () => {
            clearInterval(intervalId)
        }
    }, [])

    React.useEffect(() => {
        if (countDown <= 1) {
            clearInterval(intervalId)
            props.navigation.navigate(AppRoute.AUTH)
        }
    }, [countDown, intervalId])

    return (
        <Layout style={styles.container}>
            <Text>{'LoadingScreen'}</Text>
            <Text>{`navigation in ${countDown}`}</Text>
            <Spinner size='giant' />
        </Layout>
    )
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

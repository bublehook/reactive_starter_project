import React, { useEffect } from 'react';
import { StyleSheet, Alert } from 'react-native';
import { Layout, LayoutElement, Text, Button } from '@ui-kitten/components';

import { useStore, useStoreActions, useStoreState } from "../store";
import { useAsyncStorage } from '@react-native-community/async-storage';



export const WelcomeScreen = (props): LayoutElement => {

  const testItems = useStoreState(s => s.test.items)
  const userItems = useStoreState(s => s.test.users)
  const fetchUser = useStoreActions(a => a.test.fetch)

  const aStore = useAsyncStorage("@test")

  useEffect(() => {

    fetchUser()

    return () => {
    
    }
  } , [])

  return (
    <Layout style={styles.container}>
      <Text style={styles.text} category='h1'>   Welcome to React Navigation 5 Guide </Text>
      {testItems.map(s => (<Text>{`item : ${s}`}</Text>))}
      <Button onPress={async () => {
        try {
          await aStore.setItem(`${new Date().getTime()}`)
          Alert.alert("", "success")
        } catch (error) {
          Alert.alert("", "faild")
        }
      }} >
        SaveToAsyncStoreage
      </Button>
      <Button onPress={async () => {
        try {
          const s = await aStore.getItem()
          Alert.alert("", s + "")
        } catch (error) {
          Alert.alert("", "faild")
        }
      }} >
        GetFromAsyncStoreage
      </Button>

      {userItems.map((u) => {
        return <Text>{`name : ${u.name} createdAt : ${u.createdAt}`}</Text>
      })}



    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
  },
});

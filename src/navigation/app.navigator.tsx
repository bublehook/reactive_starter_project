import React from 'react';
import { createStackNavigator, } from '@react-navigation/stack';
import { AuthNavigator } from './auth.navigator';
import { LoadingNavigator } from './loading.navigator'
import { HomeNavigator } from './home.navigator'
import { WelcomeScreen } from '../scenes/welcome.component'

import { AppRoute } from './app-routes';

type StackNavigatorProps = React.ComponentProps<typeof Stack.Navigator>;

export type AppNavigatorParams = {
  [AppRoute.AUTH]: undefined;
  [AppRoute.HOME]: undefined;
  [AppRoute.LOADING]: undefined;
}

const Stack = createStackNavigator<AppNavigatorParams>();

export const AppNavigator = (props: Partial<StackNavigatorProps>): React.ReactElement => (
  <Stack.Navigator {...props} headerMode='none'>
    <Stack.Screen name={AppRoute.LOADING} component={LoadingNavigator} />
    <Stack.Screen name={AppRoute.AUTH} component={AuthNavigator} />
    <Stack.Screen name={AppRoute.HOME} component={HomeNavigator} />

  </Stack.Navigator>
)
import React from 'react';
import { RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AppNavigatorParams } from './app.navigator';
import { ResetPasswordScreen, SignInScreen, SignUpScreen } from '../scenes/auth';
import { LoadingScreen } from '../scenes/loading/loading.component';

type LoadingNavigatorParams = AppNavigatorParams & {
  [AppRoute.LOADING]: undefined;
}

export interface LoadingScreenProps {
  navigation: StackNavigationProp<LoadingNavigatorParams, AppRoute.LOADING>;
  route: RouteProp<LoadingNavigatorParams, AppRoute.LOADING>;
}

const Stack = createStackNavigator<LoadingNavigatorParams>();

export const LoadingNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.LOADING} component={LoadingScreen}/>
  </Stack.Navigator>
);